<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'description_menu_importer' => 'Importe un menu venant d’un autre site.',
	'entree_url_menu' => 'URL du menu',
	'nom_menu_importer' => 'Importer un menu',
);
