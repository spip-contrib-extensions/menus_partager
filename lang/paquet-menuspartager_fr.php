<?php
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// M
	'menuspartager_description' => 'Ce plugin permet de partager les menus d’un site afin de pouvoir (grâce au même plugin) les importer dans les menus d’un autre site.',
	'menuspartager_nom' => 'Menus : partage des menus',
	'menuspartager_slogan' => 'Partager et insérer des menus venant d’un autre site.',
);
